const express = require("express");
const dotenv = require("dotenv");
dotenv.config();
const cors = require("cors");

const { MongoClient, ObjectId } = require('mongodb');
const MONGO_CONNECTION = process.env.MONGO_CONNECTION;

const client = new MongoClient(MONGO_CONNECTION);
client.connect();
console.log('Connected successfully to server');
const dbName = 'products';
const db = client.db(dbName);
const collection = db.collection('books');

var bodyParser = require("body-parser");
const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
  app.use(cors());
  next();
});

app.post('/login', (req, res, next) => {
  try {
    res.send({
      token: '123mudar'
    })
  } catch (error) {
    next(error);
  }
  
});

app.get('/getAllBooks', async function (req, res, next) {
  try {
    const findResult = await collection.find({}).toArray();
    return res.json({books: findResult});
  } catch (error) {
    next(error);
  }
});

app.post('/insertBook', async function (req, res, next) {
  try {
    const bookName = req.body.bookName;
    const insertResult = await collection.insertOne({title: bookName, reviews: []});
    return res.json({books: insertResult});
  } catch (error) {
    next(error);
  }
});

app.put('/updateBook', async function(req, res, next){
  try{
    const bookId = req.body.bookId;
    const bookName = req.body.bookName;
    const updateResult = await collection.updateOne({_id: new ObjectId(bookId)}, {$set: {title: bookName}});
    return res.json({books: updateResult});
  }catch(error){
    next(error);
  }
})

app.put('/insertReview', async function(req, res, next){
  try{
    const bookId = req.body.bookId;
    const review = req.body.review;
    const updateResult = await collection.updateOne({_id: new ObjectId(bookId)}, {$push: {reviews: review}});
    return res.json({books: updateResult});
  }catch(error){
    next(error);
  }
})

app.delete('/deleteBook', async function(req, res, next){
  try{
    const bookId = req.body.bookId;
    const deleteResult = await collection.deleteOne({_id: new ObjectId(bookId)});
    return res.json({books: deleteResult});
  }catch(error){
    next(error);
  }
})

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});



// main()
//   .then(console.log)
//   .catch(console.error)
//   .finally(() => client.close());